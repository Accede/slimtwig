<?php

namespace Accede\Slim;

class HttpForbiddenException extends \Exception {
}
;
class HttpUnauthorizedException extends \Exception {
}
class Auth extends \Slim\Middleware {
	public function hasIdentity($app) {
		return Session::HasUser ();
	}
	public function isAllowed($app, $named, $privilege, $resource) {
		return false;
	}
	public function call() {
		$app = \Slim\Slim::getInstance ();
		$isAuthorized = function () use($app) {
			$currentRoute = $app->router->getCurrentRoute ();
			$resource = $currentRoute->getPattern ();
			$privilege = $app->request->getMethod ();
			
			$hasIdentity = $this->hasIdentity ( $app );
			$isAllowed = (in_array ( $currentRoute->getName (), array (
					'login',
					'logout',
					'css' 
			) ));
			
			if ($hasIdentity) {
				$isAllowed = $this->isAllowed ( $app, $currentRoute->getName (), $privilege, $resource );
			}
			
			if ($hasIdentity && ! $isAllowed) {
				throw new HttpForbiddenException ( "You do not have $privilege access to $resource" );
			}
			if (! $hasIdentity && ! $isAllowed) {
				throw new HttpUnauthorizedException ( "Authoriztion Required" );
			}
		};
		
		$app->hook ( 'slim.before.dispatch', $isAuthorized );
		$this->next->call ();
	}
}
