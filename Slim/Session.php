<?php

namespace Accede\Slim;

class Session {
	const UserId = "UserID";
	const TimeZone = "TimeZone";

	public static function Login($userid) {
		$_SESSION[Session::UserId] = $userid;

		return false;
	}

	public static function Logout() {
		if (isset($_SESSION)) {
			//just using the @ until i get time to fix this 
			@session_destroy();
		}
		return true;
	}

	public static function HasUser() {
		return !empty($_SESSION[Session::UserId]);
	}

	public static function getUserId() {
		return !empty($_SESSION[Session::UserId]) ? $_SESSION[Session::UserId] : false;
	}

	public static function localTimeZone() {
		return (isset($_SESSION[Session::TimeZone])) ? $_SESSION[Session::TimeZone] : 'UTC';
	}

	public static function setLocalTimeZone($timezone = 'UTC') {
		$_SESSION[Session::TimeZone] = $timezone;
	}

}
