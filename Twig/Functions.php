<?php

namespace Accede\Twig;

class Functions extends \Twig_Extension {

	public function getFunctions() {
		return array(new \Twig_SimpleFunction('iif', array($this, 'IIF')), new \Twig_SimpleFunction('percent', array($this, 'percent')), new \Twig_SimpleFunction('percent_class', array($this, 'percent_class')));
	}

	public function getFilters() {
		return array('json_decode' => new \Twig_Filter_Method($this, 'jsonDecode'), );
	}

	public function IIF($if, $then, $else = '') {
		return ($if) ? $then : $else;
	}

	public function percent($value, $max, $min = 0) {
		if ($max == 0) {
			return "Error";
		}
		$value = ($value / $max) * 100;

		return number_format($value);
	}

	public function percent_class($value, $max, $min = 0) {
		if ($max == 0) {
			return "info";
		}
		$value = ($value / $max) * 100;
		if ($value > 80) {
			return "success";
		}
		if ($value > 60) {
			return "info";
		}
		if ($value > 40) {
			return "warning";
		}
		return "danger";

	}

	public function getName() {
		return 'CRMMap Misc Functions Twig_Extension';
	}

    public function jsonDecode($str) {
        return json_decode($str);
    }
}
?>