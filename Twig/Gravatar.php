<?php

namespace Accede\Twig;

class Gravatar extends \Twig_Extension {
	const urlbase = "http://www.gravatar.com/avatar/";
	public function getFunctions() {
		return array (
				new \Twig_SimpleFunction ( 'GravatarMD5', array (
						$this,
						'GravatarMD5' 
				) ),
				new \Twig_SimpleFunction ( 'GravatarURL', array (
						$this,
						'GravatarURL' 
				) ) 
		);
	}
	public function GravatarMD5($email) {
		return md5 ( strtolower ( trim ( $email ) ) );
	}
	public function GravatarURL($email, $size=100) {
		return Gravatar::urlbase . Gravatar::GravatarMD5 ( $email ) . "?d=mm&s=$size";
	}
	public function getName() {
		return 'Gravatar Twig_Extension';
	}
}
?>